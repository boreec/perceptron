# Perceptron

A very simple implentation of the Perceptron on random data in Python3 as Jupyter Notebook.

The Jupyter notebook may not render properly on the repository web interface. I recommend to download it.

author : [Cyprien Borée](https://boreec.fr)